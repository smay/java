import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SqlService {
    private String databaseName;
    private String username;
    private String password;
    private Connection connection;

    public SqlService() {
        this.databaseName = "demo";
        this.username = "sa";
        this.password = "Duchai9800";
    }

    public void connection() throws SQLException, ClassNotFoundException {
        this.connection = SQLServerConnectUtils.getConnection(this.databaseName, this.username, this.password);
    }

    public ArrayList<Student> queryData() throws SQLException {
        String queryString = "SELECT * FROM student";
        Statement statement = this.connection.createStatement();
        ResultSet resultSet = statement.executeQuery(queryString);
        ArrayList<Student> students = new ArrayList<>();

        while (resultSet.next()) {
            Student student = new Student(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("gender"));
            students.add(student);
        }

        return students;
    }

    public void disconnection() throws SQLException {
        this.connection.close();
    }
}
