import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLServerConnectUtils {

    public static Connection getConnection(String databaseName, String username, String password) throws SQLException, ClassNotFoundException {
        Connection connection = null;

        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        String connectionURL = "jdbc:sqlserver://localhost:1433;databaseName="
                + databaseName;

        connection = DriverManager.getConnection(connectionURL, username, password);

        System.out.println("connect successfully!");

        return connection;
    }

}
