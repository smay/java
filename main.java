import java.sql.SQLException;
import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        SqlService sqlService = new SqlService();

        ArrayList<Student> students = null;
        try {

            sqlService.connection();
            students = sqlService.queryData();
            sqlService.disconnection();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        for (Student student: students) {
            System.out.println(student.getName());
        }
    }
}
