public class Student {
    private int id;
    private String name;
    private String gender; //male or female

    public Student(int id, String name, String genre) {
        this.id = id;
        this.name = name;
        this.gender = genre;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }
}
